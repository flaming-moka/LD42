# Demonic Digestion - LD42

Flaming Moka's entry for the 42th Ludum Dare jam. Theme: "running out of space"

**What's the best place for a demon to be, other than a bullet hell?**

Play as a *yokai* and survive as long as you can to the dangerous consequences of your hunger! 
Defeat the bigger demons and gather resources, unlock items, customize your appearance. 


Features:
- **shop** with upgrade system
- main character **customization**
- tutorial

Controls:
- select with **mouse**
- move with **WASD** or arrows
- quit game with **ESC**


Demonic Digestion is a 2d action/arcade game built with **Godot Engine**. Try it out!

## Changelog, 15/8/2018

Graphic adjustments:
- fixed several animation bugs
- improved menu legibility
- improved tutorial image quality

Game parameters adjustments:
- fixed the buttons' logic in the item menu
- calibrated values in the score system
- calibrated damage values
- calibrated items' price
- calibrated spawn intervals
- adjusted the speed of enemies and bonus items

...and a ton of bugfixes!