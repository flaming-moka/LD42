extends TextureButton

signal pressing_completed

func _ready():
	pass

func _on_Button_pressed():
	$AnimationPlayer.play("on_click")

func _on_AnimationPlayer_animation_finished(anim_name):
	emit_signal("pressing_completed")



