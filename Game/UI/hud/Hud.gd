extends Control

var seconds = 0
var minutes = 0
var menu = "res://UI/menu/Menu.tscn"

func _ready():
	pass

func _process(delta):
	$MarginContainer/Columns/Score.text = str(Global.score)
	$MarginContainer/Columns/Time.text = "%d' %d\"" % [minutes, seconds]

func _on_Timer_timeout():
	Global.time_played += 1
	seconds += 1
	if seconds == 60:
		seconds = 0
		minutes += 1

func _on_Back_pressed():
	get_tree().change_scene(menu)