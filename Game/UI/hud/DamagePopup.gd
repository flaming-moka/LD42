extends Control

func _ready():
	# play the animation
	$AnimationPlayer.play("points")

func init(pos, amount):
	# set the points' position (center of the enemy)
	self.rect_position = pos - $Label.rect_size / 2
	# set the amount of points given
	$Label.text = "-" + String(amount)

func _on_AnimationPlayer_animation_finished( name ):
	queue_free()