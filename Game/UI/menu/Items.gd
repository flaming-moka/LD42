extends MarginContainer

var equipment_displayed = true

func _process(delta):
	$HBoxContainer/VBoxContainer/Points.text = "%d" % Global.money

func _on_TextureButton_pressed():
	# show equipment
	if not equipment_displayed:
		get_node("HBoxContainer/Appearance").queue_free()
		var Equipment = load("res://UI/menu/Equipment.tscn")
		var e = Equipment.instance()
		$HBoxContainer.add_child(e)
		equipment_displayed = true

func _on_TextureButton2_pressed():
	# show appereance
	if equipment_displayed:
		get_node("HBoxContainer/Equipment").queue_free()
		var Appereance = load("res://UI/menu/Appearance.tscn")
		var a = Appereance.instance()
		$HBoxContainer.add_child(a)
		equipment_displayed = false

func _on_Back_pressed():
	get_parent().get_node("SwapAnimations").play_backwards("menu_to_items")



