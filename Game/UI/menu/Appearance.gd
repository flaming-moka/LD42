extends MarginContainer

var opacity = 0.5

func _ready():
	if Global.appereance == "fox":
		$GridContainer/HBoxContainer/Fan.modulate.a = opacity
	elif Global.appereance == "dragon":
		$GridContainer/HBoxContainer2/Sakura.modulate.a = opacity
	elif Global.appereance == "oni":
		$GridContainer/HBoxContainer3/Mask.modulate.a = opacity

func _on_Fan_pressed():
	Global.appereance = "fox"
	$GridContainer/HBoxContainer/Fan.modulate.a = opacity
	$GridContainer/HBoxContainer2/Sakura.modulate.a = 1
	$GridContainer/HBoxContainer3/Mask.modulate.a = 1

func _on_Sakura_pressed():
	Global.appereance = "dragon"
	$GridContainer/HBoxContainer2/Sakura.modulate.a = opacity
	$GridContainer/HBoxContainer3/Mask.modulate.a = 1
	$GridContainer/HBoxContainer/Fan.modulate.a = 1

func _on_Mask_pressed():
	Global.appereance = "oni"
	$GridContainer/HBoxContainer3/Mask.modulate.a = opacity
	$GridContainer/HBoxContainer2/Sakura.modulate.a = 1
	$GridContainer/HBoxContainer/Fan.modulate.a = 1
