extends MarginContainer

var opacity = 0.5
var equipped_color = Color(0.224, 0.651, 0.341, 1)

func _ready():
	if Global.attack:
		$GridContainer/row4/Shuriken.modulate.a = opacity
		$GridContainer/row4/shuriken_container/shuriken_label.modulate = equipped_color
	if Global.life:
		$GridContainer/row3/Mask.modulate.a = opacity
		$GridContainer/row3/mask_container/mask_label.modulate = equipped_color
	if Global.time:
		$GridContainer/row2/Sakura.modulate.a = opacity
		$GridContainer/row2/sakura_container/sakura_label.modulate = equipped_color
	if Global.speed:
		$GridContainer/row1/Fan.modulate.a = opacity
		$GridContainer/row1/fan_container/fan_label.modulate = equipped_color

func _on_Shuriken_pressed():
	if Global.money >= 5000 and Global.attack == false:
		$GridContainer/row4/Shuriken.modulate.a = opacity		
		$GridContainer/row4/shuriken_container/shuriken_label.modulate = equipped_color
		Global.money -= 5000
		Global.attack = true

func _on_Mask_pressed():
	if Global.money >= 20000 and Global.life == false:
		$GridContainer/row3/Mask.modulate.a = opacity
		$GridContainer/row3/mask_container/mask_label.modulate = equipped_color
		Global.money -= 20000
		Global.life = true

func _on_Sakura_pressed():
	if Global.money >= 10000 and Global.time == false:
		$GridContainer/row2/Sakura.modulate.a = opacity
		$GridContainer/row2/sakura_container/sakura_label.modulate = equipped_color
		Global.money -= 10000
		Global.time = true

func _on_Fan_pressed():
	if Global.money >= 20000 and Global.speed == false:
		$GridContainer/row1/Fan.modulate.a = opacity
		$GridContainer/row1/fan_container/fan_label.modulate = equipped_color
		Global.money -= 20000
		Global.speed = true