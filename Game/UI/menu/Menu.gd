extends Control

var level = "res://level/Level.tscn"

func _ready():
	pass

## play
func _on_Play_pressed():
	$list/rows/Play/AnimationPlayer.play("button_clicked")

func _on_Play_animation_finished(anim_name):
	get_tree().change_scene(level)

## items
func _on_Items_pressed():
	$list/rows/Items/AnimationPlayer.play("button_clicked")

func _on_Items_animation_finished(anim_name):
	$SwapAnimations.play("menu_to_items")

## tutorial
func _on_Tutorial_pressed():
	$list/rows/Tutorial/AnimationPlayer.play("button_clicked")

func _on_Tutorial_animation_finished(anim_name):
	$SwapAnimations.play("menu_to_tutorial")

## credits
func _on_Credits_pressed():
	$list/rows/Credits/AnimationPlayer.play("button_clicked")

func _on_Credits_animation_finished(anim_name):
	$SwapAnimations.play("menu_to_credits")





