extends MarginContainer

var level = "res://level/Level.tscn"
var menu =  "res://UI/menu/Menu.tscn"

func _ready():
	# store earned money
	Global.money += Global.score
	# show time and score
	$Rows/Middle/Time.text = "Time - %d' %d\"" % [int(Global.time_played / 60), int(Global.time_played % 60)]
	$Rows/Middle/Points.text = "Score - %s" % str(Global.score)

func _on_TextureButton_pressed():
	$Rows/Middle2/TextureButton/AnimationPlayer.play("button_clicked")


func _on_AnimationPlayer_animation_finished(anim_name):
	get_tree().change_scene(level)


func _on_TextureButton2_pressed():
	$Rows/Middle2/TextureButton2/AnimationPlayer2.play("button_clicked")
	
func _on_AnimationPlayer2_animation_finished(anim_name):
	get_tree().change_scene(menu)