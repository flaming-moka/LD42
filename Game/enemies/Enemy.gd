extends Area2D

signal enemy_dead
signal enemy_spawned

export var max_life = 5
export var speed = 10
export var points = 100
export (PackedScene) var Points
export (PackedScene) var DamagePopup
export var deceleration = 0.5

var extents
var life
var dir = Vector2(-1, 0)

func _ready():
	Global.enemy_alive = true
	self.connect("enemy_dead", get_parent().get_parent(), "restore_game_song")
	self.connect("enemy_spawned", get_parent().get_parent().get_node("Player"),
				"spawn_shuriken")
	
	max_life += (80 * Global.enemy_count)
	life = max_life
	Global.enemy_count += 1

	var size = $animation_root/body.texture.get_size() + $animation_root/body/neck/head.texture.get_size()
	extents = size / 2
	global_position.x = Global.screensize.x + extents.x + 100
	global_position.y = Global.screensize.y / 2 + 130
	# player perk
	if Global.time:
		speed -= (speed / 3)
	
	if Global.attack:
		emit_signal("enemy_spawned", global_position)

func _physics_process(delta):
	global_position += dir * speed * deceleration * delta
	if global_position.x < -extents.x:
		call_deferred("queue_free")

func display_damage(pos, damage):
	var d = DamagePopup.instance()
	d.init(pos, damage)
	get_parent().add_child(d)

func take_damage(amount):
	life -= amount
	if life > 0:
		$AnimationPlayer.play("enemy_hit")
	else:
		emit_signal("enemy_dead")
		Global.enemy_alive = false
		Global.max_skulls_alive += 2
		# give points
		var p = Points.instance()
		p.init(global_position - Vector2(400, 0), points)
		p.get_node("Label").rect_size *= Vector2(5, 5)
		get_parent().get_parent().get_node("PointsContainer").add_child(p)
		if Global.was_active:
			# reload shuriken
			Global.attack = true
		queue_free()

func _on_Enemy_area_entered(area):
	if area.has_method("take_damage"):
		area.take_damage()
