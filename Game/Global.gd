extends Node2D

onready var screensize = get_viewport_rect().size
# important parameters
var score = 0
var time_played = 0
var money = 0
var enemy_count = 0
var enemy_alive = false
var max_skulls_alive = 11
# perks
var attack = false
var life = false
var speed = false
var time = false
var was_active = false # referred to the shuriken
# customization
var appereance = "fox" # default

func _ready():
	OS.window_fullscreen = true

func _unhandled_input(event):
	if Input.is_action_pressed("escape"):
		get_tree().quit()