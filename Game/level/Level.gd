extends Node

export (PackedScene) var Food
export (PackedScene) var Enemy
export var spawn_rate = 10
export (int) var bonus_spawn_rate = 15
export (int) var max_balls = 15

onready var screensize = Global.screensize
var food_count = 0
var bonuses = {"res://collectables/bonus/Life.tscn" : 70,
				"res://collectables/bonus/Luck.tscn" : 70,
				"res://collectables/bonus/Protection.tscn" : 50,
				"res://collectables/bonus/Damage.tscn" : 50, 
				"res://collectables/bonus/Time.tscn" : 30}

func _ready():
	randomize()
	# init game variables
	Global.score = 0
	Global.time_played = 0
	# init spawning of things
	food_count = randi() % 1 + 5
	spawn_food(food_count)
	$SpawnTimer.wait_time = spawn_rate
	$SpawnTimer.start()
	$BonusTimer.wait_time = bonus_spawn_rate
	$GameLoop.play()

func spawn_food(number):
	for f in range(number):
		var food = Food.instance()
		$FoodContainer.add_child(food)
		var random = randi() % 4
		if random == 0: # left
			food.global_position.x = rand_range(50, 350)
			food.global_position.y = rand_range(20, screensize.y - 20)
		elif random == 1: # right
			food.global_position.x = rand_range(screensize.x - 350, screensize.x -50)
			food.global_position.y = rand_range(20, screensize.y - 20)
		elif random == 2: #top
			food.global_position.x = rand_range(50, screensize.x - 50)
			food.global_position.y = rand_range(20, 200)
		else: # bottom
			food.global_position.x = rand_range(50, screensize.x - 50)
			food.global_position.y = rand_range(screensize.y - 200, screensize.y - 20)

func _on_SpawnTimer_timeout():
	# stop spawning food if there are too many balls
	if $BallContainer.get_child_count() >= max_balls:
		return
	# increase spawn rate
	if spawn_rate > 1:
		spawn_rate -= 1
	# spawn food
	spawn_food(1)

func _on_BonusTimer_timeout():
	spawn_bonus()
	# get current number on balls to tweak bonus spawn
	var cur_ball_count = $BallContainer.get_child_count()
	$BonusTimer.start()

func spawn_bonus():
	# choose the item
	var tot_weight = 0
	for key in bonuses.keys():
		tot_weight += bonuses[key]
	var rand_number = rand_range(0, tot_weight)
	var item
	for key in bonuses.keys():
		if rand_number < bonuses[key]:
			item = key
			break
		rand_number -= bonuses[key]
	# spawn it
	var Bonus = load(item)
	var b = Bonus.instance()
	b.global_position = Vector2(rand_range(30, screensize.x - 30),
								rand_range(30, screensize.y - 30))
	$BonusContainer.add_child(b)

func _on_EnemyTimer_timeout():
	# check if enemy can spawn
	var tot_skulls = 0
	for child in $BallContainer.get_children():
		if "Ball" in child.name:
			tot_skulls += 1
	if tot_skulls < Global.max_skulls_alive or Global.enemy_alive:
		return 
	# start fading out gameloop song
	$GameLoop/Tween.interpolate_property($GameLoop, "volume_db", $GameLoop.volume_db, -50, 6.0,
										Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$GameLoop/Tween.start()
	# fade in drums
	$Drums.play()
	$Drums/Tween.interpolate_property($Drums, "volume_db", $Drums.volume_db, -10, 6.0,
										Tween.TRANS_LINEAR, Tween.EASE_IN)
	$Drums/Tween.start()
	# spawn the enemy
	var e = Enemy.instance()
	$EnemyContainer.add_child(e)

# called when the enemy dies
func restore_game_song():
	# fade out enemy song
	$Drums/Tween.interpolate_property($Drums, "volume_db", $Drums.volume_db, -50, 6.0,
										Tween.TRANS_LINEAR, Tween.EASE_OUT)
	$Drums/Tween.start()
	# fade in gameloop song
	$GameLoop/Tween.interpolate_property($GameLoop, "volume_db", $GameLoop.volume_db, 0, 6.0,
										Tween.TRANS_LINEAR, Tween.EASE_IN)
	$GameLoop/Tween.start()
	# restart enemy timer
	$EnemyTimer.start()



