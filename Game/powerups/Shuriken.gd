extends Area2D

export var speed = 300
export (int) var damage = 150
var angle = 0
var distance = Vector2()
var target_pos = Vector2()

func _ready():
	distance = target_pos - global_position
	angle = distance.angle()
	$ShotSound.play()

func _physics_process(delta):
	global_position += Vector2(speed, 0).rotated(angle) * delta

func _on_Shuriken_area_entered(area):
	if area.name == "Enemy":
		area.take_damage(damage)
		hide()
		$HitSound.play()

func _on_HitSound_finished():
	queue_free()


