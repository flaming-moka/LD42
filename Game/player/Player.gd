extends Area2D

export var speed = 500
export (float) var friction = -1
export (PackedScene) var Ball
export (PackedScene) var Points
export (PackedScene) var Shuriken
export (PackedScene) var GameOver
export (int) var max_life = 3
export (int) var katana_damage = 100

onready var life
var screensize = Vector2()
var dir = Vector2()
var vel = Vector2()
var last_rot = 0

var mouth_open = false
var protected = false
var invulnerable = false
var a = Global.appereance

func _ready():
	screensize = get_viewport_rect().size
	global_position = screensize / 2
	if Global.life:
		max_life += 1
		$Life.show()
	if Global.speed:
		var speedup = speed / 3
		speed += speedup
		$Speed.show()
	if Global.time:
		$Time.show()

	life = max_life
	# load appereance
	$Sprite.animation = "%s_idle" % a
	$Jaw.texture = load("res://Art/player/%s_jaw.png" % a)
	if a == "dragon":
		$Jaw.offset = Vector2(0, 65)
	elif a == "fox" or a == "oni":
		$Jaw.offset = Vector2(0, 35)

func _unhandled_key_input(event):
	if event.is_action_pressed("ui_left"):
		last_rot = PI
	if event.is_action_pressed("ui_right"):
		last_rot = 0
	if event.is_action_pressed("ui_up"):
		last_rot = PI/2
	if event.is_action_pressed("ui_down"):
		last_rot = 3*PI/2

func _physics_process(delta):
	dir = Vector2()
	if Input.is_action_pressed("ui_left"):
		dir.x = -1
		rotation = PI - PI/2
	if Input.is_action_pressed("ui_right"):
		dir.x = 1
		rotation = 0  - PI/2
	if Input.is_action_pressed("ui_up"):
		dir.y = -1
		rotation = 3*PI/2 - PI/2
	if Input.is_action_pressed("ui_down"):
		dir.y = 1
		rotation = PI/2 - PI/2

	dir += vel * friction
	vel += dir * delta
	global_position += vel * speed * delta
	
	if global_position.x > screensize.x:
		global_position.x = 0
	if global_position.x < 0:
		global_position.x = screensize.x
	if global_position.y > screensize.y:
		global_position.y = 0
	if global_position.y < 0:
		global_position.y = screensize.y

func smooth_rot(rot):
	if not $Tween.is_active():
		$Tween.interpolate_property($Sprite, "rotation", last_rot, rot, 0.5,
									Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.start()

func _on_Player_area_entered(area):
	if "Collectable" in area.name:
		# spawn the ball
		var b = Ball.instance()
		b.global_position = $Sprite/TailPos.global_position
		b.initial_rot(rotation - PI/2)
		get_parent().get_node("BallContainer").add_child(b)
		# play the sound
		$Bite.play()
		# open the mouth
		if not mouth_open:
			$Jaw.offset += Vector2(0, 15)
			$JawTimer.start()
			mouth_open = true
		# play the eaten animation
		area.play_anim()
	if "Life" in area.name or "Luck" in area.name or "Time" in area.name:
		$Bonus1.play()
	if "Damage" in area.name or "Protection" in area.name:
		$Bonus2.play()
	if "Ball" in area.name:
		$SkullHit.play()
		if $Katana.visible:
			$Katana/SwordHit.play()
			$Katana.call_deferred("set_monitoring", false)
			$Katana.hide()
			protected = false
		area.die()
	# give points for food and bonuses
	if not "Ball" in area.name:
		var p = Points.instance()
		p.init(global_position, area.points)
		get_parent().get_node("PointsContainer").add_child(p)

func take_damage():
	if invulnerable:
		return
	# check protection
	print(protected)
	if not protected:
		life -= 1
		# check mask
		if $Life.visible:
			$Life.hide()
		if life == 0:
			$Sprite.animation = "%s_dead" % a
			$AnimationPlayer.play("player_dead")
			speed = 0
			$Timer.start()
		else:
			# display hit animation, change trail color, make invulnerable
			$AnimationPlayer.play("player_hit")
			$Sprite.animation = "%s_hit" % a
			set_particle()
			invulnerable = true
			call_deferred("set_monitoring", false)
	else:
		protected = false
		$Protection.hide()

func set_particle():
	if life < 1 or life > 3:
		return
	var texture_path = "res://Art/trail_%d.png" % life
	var material_path = "res://player/player_material_%d.tres" % life
	$Particles2D.texture = load(texture_path)
	$Particles2D.process_material = load(material_path)

func show_game_over():
	var g = GameOver.instance()
	get_parent().add_child(g)
	# stop all timers
	for node in get_parent().get_children():
		if "Timer" in node.name:
			node.stop()
	get_parent().get_node("Hud/Timer").stop()

func get_katana():
	protected = true
	$Katana.call_deferred("set_monitoring", true)
	$Katana.show()

func heal():
	if life < max_life:
		life += 1
		set_particle()
	if max_life == 4 and life == max_life:
		print("life: ", life)
		$Life.show()

func set_protected():
	protected = true
	$Protection.show()

func _on_AnimationPlayer_animation_finished(anim_name):
	invulnerable = false
	call_deferred("set_monitoring", true)
	$Sprite.animation = "%s_idle" % a

func _on_Katana_area_entered(area):
	if area.name == "Enemy":
		$Katana/SwordHit.play()
		area.take_damage(katana_damage)
		area.display_damage(global_position, katana_damage)
		$Katana.call_deferred("set_monitoring", false)
		$Katana.hide()

func spawn_shuriken(enemy_pos):
	yield(get_tree().create_timer(12.0), "timeout")
	var s = Shuriken.instance()
	s.global_position = global_position
	s.target_pos = enemy_pos
	get_parent().add_child(s)
	Global.attack = false
	Global.was_active = true

func _on_JawTimer_timeout():
	$Jaw.offset -= Vector2(0, 15)
	mouth_open = false

func _on_Timer_timeout():
	show_game_over()
	queue_free()









