extends "res://collectables/bonus/Bonus.gd"

func _ready():
	points = 300

func _on_Life_area_entered(area):
	if area.name == "Player":
		area.heal()
		Global.score += 300
		queue_free()