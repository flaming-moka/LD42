extends Area2D

var speed = 70
var angle = 0
var start_pos = Vector2()
onready var screensize = Global.screensize
var points = 0

func _ready():
	var target_pos = screensize / 2
	# random spawn
	randomize()
	if randi() % 1 == 0:
		# left side
		start_pos = Vector2(-50, rand_range(20, Global.screensize.y - 20))
	else:
		# right side
		start_pos = Vector2(Global.screensize.x + 50, rand_range(20, Global.screensize.y - 20))
	# set the angle
	global_position = start_pos
	angle = global_position.angle_to_point(target_pos) - PI
	angle += rand_range(-0.1, 0.1)

func _physics_process(delta):
	global_position += Vector2(speed, 0).rotated(angle) * delta
	# screen wrapping
	if global_position.x > screensize.x:
		global_position.x = 0
	if global_position.x < 0:
		global_position.x = screensize.x
	if global_position.y > screensize.y:
		global_position.y = 0
	if global_position.y < 0:
		global_position.y = screensize.y