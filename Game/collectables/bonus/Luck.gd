extends "res://collectables/bonus/Bonus.gd"

export var score_multiplier = 1.2

func _ready():
	points = int(Global.score * score_multiplier) - Global.score

func _on_Luck_area_entered(area):
	if area.name == "Player":
		Global.score = int(Global.score * score_multiplier)
		queue_free()