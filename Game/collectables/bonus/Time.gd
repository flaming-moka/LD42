extends "res://collectables/bonus/Bonus.gd"

export var time = 4
var ball_speeds = []

func _ready():
	points = 500
	randomize()
	$Timer.wait_time = time

func _on_Time_area_entered(area):
	Global.score += 500
	if area.name == "Player":
		for child in get_parent().get_parent().get_node("BallContainer").get_children():
			# get the current ball speeds
			ball_speeds.append(child.speed)
			# slow the speed
			child.speed -= (child.speed / 5)
		call_deferred("set_monitoring", false)
		hide()
		$Timer.start()

func _on_Timer_timeout():
	# restore balls' speed
	var balls = get_parent().get_parent().get_node("BallContainer").get_children()
	for i in balls.size():
		if i >= ball_speeds.size():
			break
		else:
			balls[i].speed = ball_speeds[i]
	queue_free()
