extends "res://collectables/bonus/Bonus.gd"

func _ready():
	points = 300

func _on_Damage_area_entered(area):
	if area.name == "Player":
		area.get_katana()
		Global.score += 300
		queue_free()