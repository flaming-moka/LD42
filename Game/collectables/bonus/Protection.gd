extends "res://collectables/bonus/Bonus.gd"

func _ready():
	points = 500

func _on_Protection_area_entered(area):
	if area.name == "Player":
		area.set_protected()
		Global.score += 500
		queue_free()
