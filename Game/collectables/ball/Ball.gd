extends Area2D

export var speed = 300
export var maxspeed = 600
export (int) var damage = 10

var screensize
var vel = Vector2()
var rot = 0
var extents = Vector2()
var new_speed = 0
var max_bounces = 10
var bounces = 0

func _ready():
	extents = ($Sprite.texture.get_size() * $Sprite.scale) / 4
	screensize = get_viewport_rect().size
	randomize()
	new_speed = speed - (speed / 5)
	# player perk
	if Global.time:
		speed = new_speed

func _physics_process(delta):
	vel = Vector2(speed, 0).rotated(rot) 
	global_position += vel * delta

func initial_rot(angle):
	rot = angle

func bounce(collision_pos):
	rot -= rand_range(PI/2, PI)
	speed *= 1.05
	speed = clamp(speed, 0, maxspeed)
	# bounce multiplier
	bounces += 1
	if bounces <= max_bounces:
		damage += 10

func _on_Ball_area_entered(area):
	if area.name == "Player":
		area.take_damage()
	if area.name == "Enemy":
		area.take_damage(damage)
		area.display_damage(global_position, damage)
		self.die()
	if "Ball" in area.name:
		bounce(global_position)

func _on_Ball_body_entered(body):
	if body.collision_layer == 64:
		bounce(global_position)

func die():
	speed = 0
	$AnimationPlayer.play("skull_explosion")
	$AnimatedSprite.animation = "collision"

func _on_AnimationPlayer_animation_finished(anim_name):
	queue_free()

func _on_VisibilityNotifier2D_screen_exited():
	queue_free()
