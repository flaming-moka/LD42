extends Area2D

export var points = 100
export var speed = 20
export var acc = -0.5

onready var screensize = Global.screensize

func _physics_process(delta):
	var vel = Vector2(-speed, 0).rotated(compute_angle())
	global_position += vel * acc * delta

func compute_angle():
	var distance = (screensize / 2) - global_position
	var angle = distance.angle()
	return angle + PI/2

func play_anim():
	$Sprite.hide()
	$AnimatedSprite.animation = "aura_eaten"
	$Timer.start()

func _on_Timer_timeout():
	queue_free()

